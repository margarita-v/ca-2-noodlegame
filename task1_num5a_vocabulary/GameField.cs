﻿using System.Collections.Generic;
using task2_num10_noodle_game.Dict;

namespace task2_num10_noodle_game
{
    // Класс GameField описывает поле для игры и основную логику работы с ним
    internal class GameField
    {
        // размер поля
        private int _size;
        // начальное слово
        private string _initialWord;
        // поле для игры
        private char[,] _field;
        // номер строки, на которой расположено начальное слово и с которой будет начинаться поиск новых слов
        private int _startPosition;
        // количество ходов для получения всех искомых слов
        private int _countMoves;
        // количество букв на поле
        public int CountLetters { get; private set; }

        // все слова, которые можно получить через _countMoves ходов
        private TrieTree _result;
        // возможные перемещения по горизонтали 
        private static readonly int[] Dx = { 1, 0, -1, 0 };
        // возможные перемещения по вертикали
        private static readonly int[] Dy = { 0, 1, 0, -1 };
        // всевозможные слова, которые можно составить из заполненных ячеек поля
        private List<Stack<Cell>> _wordsOnField;

        // вспомогательная структура для решения задачи, представляет собой ячейку поля
        struct Cell
        {
            public int X { get; private set; }
            public int Y { get; private set; }
            public char Letter { get; private set; }

            public Cell(int x, int y, char letter)
            {
                X = x;
                Y = y;
                Letter = letter;
            }
        }

        // функция изменения размера поля; при изменении размера меняется и начальное слово
        public void Resize(int newSize, string newWord, int countMoves)
        {
            if (newWord.Length == newSize)
            {
                _size = newSize;
                _initialWord = newWord;
                _field = new char[_size, _size];
                _startPosition = _size / 2;
                _countMoves = countMoves;
                CountLetters = _size;
                // записываем в поле начальное слово
                for (int i = 0; i < _size; ++i)
                    _field[_startPosition, i] = _initialWord[i];
            }
        }

        public void ChangeParams(string newWord, int countMoves)
        {
            if (newWord.Length == _size)
            {
                _initialWord = newWord;
                _startPosition = _size / 2;
                _countMoves = countMoves;
                //CountLetters = _size;
                // записываем в поле начальное слово
                for (int i = 0; i < _size; ++i)
                    _field[_startPosition, i] = _initialWord[i];
            }
        }

        // функция добавления буквы на поле игры
        public void AddLetter(int i, int j, char letter)
        {
            _field[i, j] = letter;
            CountLetters++;
        }

        // функция проверки, можно ли заполнить ячейку поля с индексами i и j
        // клетку с текущими индексами можно заполнить, если заполнена хотя бы одна смежная клетка
        public bool CanWrite(int i, int j)
        {
            return (i > 0 && j > 0 && i < _size - 1 && j < _size - 1 &&
                (_field[i - 1, j] != '\0' || _field[i + 1, j] != '\0' ||
                 _field[i, j - 1] != '\0' || _field[i, j + 1] != '\0') ||

                i == 0 && j > 0 && j < _size - 1 &&
                (_field[i, j - 1] != '\0' || _field[i, j + 1] != '\0' || _field[1, j] != '\0') ||

                i == _size - 1 && j > 0 && j < _size - 1 &&
                (_field[i, j - 1] != '\0' || _field[i - 1, j] != '\0' || _field[i, j + 1] != '\0') ||

                j == 0 && i > 0 && i < _size - 1 &&
                (_field[i + 1, 0] != '\0' || _field[i, 1] != '\0' || _field[i - 1, 0] != '\0') ||

                j == _size - 1 && i > 0 && i < _size - 1 &&
                (_field[i, j - 1] != '\0' || _field[i + 1, j] != '\0' || _field[i - 1, j] != '\0') ||

                i == 0 && j == 0 && (_field[1, 0] != '\0' || _field[0, 1] != '\0') ||
                i == 0 && j == _size - 1 && (_field[0, j - 1] != '\0' || _field[1, j] != '\0') ||
                i == _size - 1 && j == 0 && (_field[i - 1, 0] != '\0' || _field[i, 1] != '\0') ||
                i == _size - 1 && j == _size - 1 && (_field[i, j - 1] != '\0' || _field[i - 1, j] != '\0')) ;
        }

        // функция получения слова из стека
        private static string GetWord(Stack<Cell> word)
        {
            // получаем все буквы из стека
            string wrd = "";
            foreach (var cell in word)
                wrd += cell.Letter;
            // переворачиваем строку, так как слова в стеке хранятся наоборот
            string result = "";
            for (int i = wrd.Length - 1; i >= 0; i--)
                result += wrd[i];
            return result;
        }

        // функция, которая проверяет, содержится ли в текущем слове ячейка с координатам x и y
        private static bool Contains(Stack<Cell> word, int x, int y)
        {
            bool found = false;
            foreach (var cell in word)
            {
                found = cell.X == x && cell.Y == y;
                if (found)
                    break;
            }
            return found;
        }

        // функця генерации всевозможных слов из заполненных ячеек поля
        private void _getAllWordsOnField(int x, int y, int currentCountMoves, int maxCountOfMoves, Stack<Cell> word)
        {
            // старые координаты
            int xOld = x;
            int yOld = y;

            if (!Contains(word, x, y))
                word.Push(new Cell(x, y, _field[x, y]));

            if (FormMain.Dict.FindByFirstSymbols(GetWord(word)))
            {
                currentCountMoves++;

                // если еще не сделали максимальное количество ходов
                if (currentCountMoves < maxCountOfMoves)
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        // новые координаты
                        int xNew = xOld + Dx[i];
                        int yNew = yOld + Dy[i];
                        // если новые координаты некорректны, переходим к следующей итерации
                        if (xNew >= _size || xNew < 0 || yNew >= _size || yNew < 0)
                            continue;
                        // если в ячейке поля с новыми координатами есть буква
                        // и если ячейка с данными координатами не была добавлена в стек
                        if (_field[xNew, yNew] != '\0' && !Contains(word, xNew, yNew))
                        {
                            _getAllWordsOnField(xNew, yNew, currentCountMoves, maxCountOfMoves, word);
                            // при выходе из рекурсии возвращаем слово в исходное состояние
                            word.Pop();
                        }
                    } // for
                }
                else // иначе добавляем слово в словарь
                {
                    var ar = word.ToArray();
                    Stack<Cell> res = new Stack<Cell>();
                    for (int i = ar.Length - 1; i >= 0; i--) 
                        res.Push(ar[i]);
                    _wordsOnField.Add(res);
                }
            } // if
        }

        // функция, которая возвращает все слова, которые можно получить через _countMoves ходов
        public TrieTree GetSolve()
        {
            _result = new TrieTree();
            _wordsOnField = new List<Stack<Cell>>();
            // промежуточный стек ячеек, которые входят в текущее слово
            Stack<Cell> stack = new Stack<Cell>();
            
            // получаем список всех занятых ячеек поля
            List<Cell> list = new List<Cell>();
            for(int i = 0; i < _size; i++)
                for (int j = 0; j < _size; j++)
                    if (_field[i, j] != '\0')
                        list.Add(new Cell(i, j, _field[i, j]));

            // для каждой занятой ячейки формируем слова всевозможной длины
            foreach (var cell in list)
                for (int i = 1; i <= CountLetters; i++)
                {
                    stack.Clear();
                    _getAllWordsOnField(cell.X, cell.Y, 0, i, stack);
                }

            // для каждого стека ячеек поля пытаемся составить слово
            for (int i = 0; i < _wordsOnField.Count; i++)
            {
                Cell lastCell = _wordsOnField[i].Peek();
                _solve(lastCell.X, lastCell.Y, 0, _countMoves, _wordsOnField[i]);
            }
            return _result;
        }

        // функця генерации всех слов, которые можно получить через _countMoves ходов
        private void _solve(int x, int y, int currentCountMoves, int maxCountOfMoves, Stack<Cell> word)
        {
            // старые координаты
            int xOld = x;
            int yOld = y;

            // если еще не сделали максимальное количество ходов
            if (currentCountMoves < maxCountOfMoves)
            {
                for (int i = 0; i < 4 && currentCountMoves <= maxCountOfMoves; ++i)
                {
                    // новые координаты
                    int xNew = xOld + Dx[i];
                    int yNew = yOld + Dy[i];
                    // если новые координаты некорректны, переходим к следующей итерации
                    if (xNew >= _size || xNew < 0 || yNew >= _size || yNew < 0)
                        continue;
                    // если в ячейке поля с новыми координатами есть буква
                    if (_field[xNew, yNew] != '\0')
                    {
                        // если ячейка с данными координатами не была добавлена в стек
                        if (!Contains(word, xNew, yNew))
                        {
                            word.Push(new Cell(xNew, yNew, _field[xNew, yNew]));
                            // если в словаре есть слова, которые начинаются на текущую комбинацию букв,
                            // и если в списке начальных комбинаций букв нет текущей комбинации
                            if (FormMain.Dict.FindByFirstSymbols(GetWord(word)) && !_wordsOnField.Contains(word))
                                _solve(xNew, yNew, currentCountMoves, maxCountOfMoves, word);
                            word.Pop();
                        }
                    }
                    else // пытаемся добавить буквы алфавита и получить новые слова
                        for (int index = 0; index < Node.Range && currentCountMoves <= maxCountOfMoves; index++)
                        {
                            char letter = Node.GetSymb(index);
                            word.Push(new Cell(xNew, yNew, letter));
                            string wrd = GetWord(word);
                            // если слов, начинающихся с текущей комбинации букв, нет в словаре
                            if (!FormMain.Dict.FindByFirstSymbols(wrd))
                            {
                                // удаляем букву и переходим к следующей итерации
                                word.Pop();
                                continue;
                            }

                            // найдена комбинация букв, с которых начинаются слова в словаре

                            // увеличиваем количество добавленных букв в пустые ячейки
                            currentCountMoves++;

                            _solve(xNew, yNew, currentCountMoves, maxCountOfMoves, word);
                                    
                            word.Pop();
                            currentCountMoves--;
                        }
                } // for
            }
            else // иначе добавляем слово в словарь
            {
                string wrd = GetWord(word);
                if (FormMain.Dict.Find(wrd) && !_result.Find(wrd))
                    _result.Add(wrd);
            }
        } // _solve
    }
}
