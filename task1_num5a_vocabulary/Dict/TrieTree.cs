﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace task2_num10_noodle_game.Dict
{
    /// <summary>
    /// Класс Trie-Tree содержит корень дерева и вызывает все рекурсивные методы для работы с ним
    /// </summary>
    public class TrieTree
    {
        private Node _root; // корень Trie-дерева

        public TrieTree() { _root = null; } 

        /// <summary>
        /// Очистка дерева
        /// </summary>
        public void Clear()
        {
            _root = null;
        } 

        /// <summary>
        /// Добавляет слово в Trie дерево, если его ещё не было и оно удовлетворяет языку структуры
        /// </summary>
        /// <param name="wrd">Добавляемое слово</param>
        /// <returns>TRUE, если добавление прошло успешно</returns>
        public bool Add(string wrd)
        {
            if (!CheckWord(wrd))
                return false;
            if (_root == null)
                _root = new Node();
            return _root.Add(ref wrd);
        } 

        /// <summary>
        /// Поиск слова в Trie-дереве
        /// </summary>
        /// <param name="wrd">Искомое слово</param>
        /// <returns>TRUE, если слово найдено, FALSE в противном случае</returns>
        public bool Find(string wrd)
        {
            if (_root == null || !CheckWord(wrd))
                return false;
            return _root.Find(wrd);
        }

        /// <summary>
        /// Поиск слов, которые начинаются с заданной последовательности символов
        /// </summary>
        /// <param name="symb">Последовательность символов, с которой должны начинаться слова</param>
        /// <returns>TRUE, если найдены слова, начинающиеся с заданных символов, FALSE в противном случае</returns>
        public bool FindByFirstSymbols(string symb)
        {
            if (_root == null || !CheckWord(symb))
                return false;
            return _root.FindByFirstSymbols(symb, 0);
        }

        /// <summary>
        /// Удаление слова из Trie-дерева
        /// </summary>
        /// <param name="wrd">Удаляемое слово</param>
        /// <returns>TRUE, если слово удалено, FALSE в противном случае</returns>
        public bool Delete(string wrd)
        {
            if (_root == null || !CheckWord(wrd))
                return false;
            if (_root.Delete(wrd))
            {
                if (_root.IsEmpty())
                    _root = null;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Загрузка словаря из файла в Trie-дерево.
        /// Будет осуществлена проверка загружаемых слов, в случае некорректных данных дерево останется пустым.
        /// </summary>
        /// <param name="filename">Имя файла, откуда будет происходить загрузка</param>
        /// <returns>TRUE, если загрузка прошла успешно, FALSE в противном случае</returns>
        public bool LoadFromFile(string filename)
        {
            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding(1251));

            _root = new Node();
            bool res = true;

            // пока не закончился файл и загруженные данные корректны
            while (sr.Peek() >= 0 && res)
            {
                var tmp = sr.ReadLine()?.ToLower().Trim();
                res = CheckWord(tmp);
                if (res)
                    _root.Add(ref tmp);
            } 

            if (!res)
                Clear();
            sr.Close();
            return res;
        } 

        /// <summary>
        /// Сохранение данных из Trie-дерева в файл
        /// </summary>
        /// <param name="filename">Имя файла, в который будет произведено сохранение</param>
        public void SaveToFile(string filename)
        {
            StreamWriter sw = new StreamWriter(filename, false, Encoding.GetEncoding(1251));
            if (_root == null || _root.IsEmpty())
            {
                sw.Close();
                return;
            }

            List<string> sl = Show();
            foreach (string t in sl)
                sw.WriteLine(t);
            sw.Close();
        }

        /// <summary>
        /// Проверка, пусто ли дерево
        /// </summary>
        /// <returns>TRUE, если дерево пусто, FALSE в противном случае</returns>
        public bool IsEmpty()
        {
            if (_root == null)
                return true;
            return _root.IsEmpty();
        } 

        /// <summary>
        /// Функция возвращает список всех слов в Trie-дереве
        /// </summary>
        /// <returns>Список всех слов Trie-дерева</returns>
        public List<string> Show()
        {
            List<string> res = new List<string>();
            if (_root == null)
                return res;

            _root.Show(res);
            return res;
        } 

        /// <summary>
        /// Функция возвращает список слов, содержащих только определенный набор символов из заданного множества
        /// </summary>
        /// <param name="wordSet">Строка, содержащая символы множества</param>
        /// <returns>Список слов, состоящих только из символов заданного множества</returns>
        public List<string> ShowBySet(string wordSet)
        {
            List<string> res = new List<string>();
            if (_root == null || !CheckWord(wordSet))
                return res;

            HashSet<char> set = new HashSet<char>(wordSet);
            _root.ShowSet(set, res);
            return res;
        }

        /// <summary>
        /// Функция отображает список всех слов в Trie-дереве на компонент формы
        /// </summary>
        public void Show(TextBox tb)
        {
            tb.Clear();
            tb.Lines = Show().ToArray();
        }

        /// <summary>
        /// Функция, которая проверяет, соответствует ли строка языку данной структуры, т.е. содержит ли корректные символы.
        /// </summary>
        /// <param name="wrd">Проверяемое на корректность слово</param>
        /// <returns>TRUE, если слово корректно, FALSE в противном случае</returns>
        public static bool CheckWord(string wrd)
        {
            if (wrd == string.Empty)
                return false;
            wrd = wrd.ToLower();
            foreach (char t in wrd)
                if (t < (char)Node.FirstLitera || t > (char)Node.LastLitera)
                    return false;
            return true;
        } 
    } 
}
