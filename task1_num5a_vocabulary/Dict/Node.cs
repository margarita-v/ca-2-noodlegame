﻿using System.Collections.Generic;

namespace task2_num10_noodle_game.Dict
{
    /// <summary>
    /// Класс Node определяет узел trie-дерева: его свойства и поведение          
    /// </summary>
    internal class Node
    {
        public static int LastLitera = 'я';                      // код последней буквы алфавита
        public static int FirstLitera = 'а';                     // код первой буквы алфавита
        public static int Range = LastLitera - FirstLitera + 1;  // диапазон букв

        public bool Point { get; set; }                         // признак конца слова
        private readonly Node[] _next;                          // ссылки на следующие буквы


        /// <summary> Индексатор для обращения к следующей букве слова (следующему звену)
        /// </summary>
        /// <param name="i">Индекс, который следует поставить в соответствие букве алфавита</param>
        /// <returns>Следующий узел trie-дерева</returns>
        public Node this[int i]
        {
            get { return _next[i]; }
            set { _next[i] = value; }
        } 

        /// <summary> Конструктор по умолчанию
        /// </summary>
        public Node()
        {
            _next = new Node[Range];
            for (int i = 0; i < Range; i++)
                _next[i] = null;
            Point = false;
        } 

        /// <summary> Проверка дерева на пустоту
        /// </summary>
        /// <returns>TRUE, если пусто, FALSE в противном случае</returns>
        public bool IsEmpty()
        {
            if (Point)
                return false;
            for (int i = 0; i < Range; i++)
                if (_next[i] == null)
                    return false;
            return true;
        } 

        /// <summary>
        /// Добавление слова в дерево
        /// </summary>
        /// <param name="wrd">Добавляемое слово</param>
        /// <returns>TRUE, если добавление прошло успешно, FALSE в противном случае</returns>
        public bool Add(ref string wrd)
        {
            if (wrd.Equals(string.Empty))
            {
                bool res = !Point; // если такое слово уже есть, возвращаем false. 
                Point = true;
                return res; // True только в случае, если такого слова не было
            }
            int first = GetIndex(wrd[0]);
            wrd = wrd.Remove(0, 1);
            if (_next[first] == null)
                _next[first] = new Node();
            return _next[first].Add(ref wrd);
        }

        /// <summary>
        /// Поиск слова в структуре TrieTree
        /// </summary>
        /// <param name="wrd">Искомое слово</param>
        /// <returns>TRUE, если слово найдено, FALSE в противном случае</returns>
        public bool Find(string wrd)
        {
            if (wrd.Equals(string.Empty)) // если слово закончилось, то проверяем, является ли узел точкой
                return Point;
            if (_next[GetIndex(wrd[0])] == null)
                return false;
            return _next[GetIndex(wrd[0])].Find(wrd.Remove(0, 1));
        }

        /// <summary>
        /// Поиск слов, которые начинаются с последовательности символов symb
        /// </summary>
        /// <param name="symb">Последовательность символов, с которой должны начинаться слова</param>
        /// <param name="index">Индекс символа исходной последовательности. При первом вызове index должен быть равен 0</param>
        /// <returns>TRUE, если найдены слова, начинающиеся с заданных символов, FALSE в противном случае</returns>
        public bool FindByFirstSymbols(string symb, int index)
        {
            if (index == symb.Length) // если дошли до конца, то слова, которые начинаются с данных символов, есть
                return true;
            if (_next[GetIndex(symb[index])] == null)
                return false;
            return _next[GetIndex(symb[index])].FindByFirstSymbols(symb, index + 1);
        }

        /// <summary>
        /// Удаление слова из дерева
        /// </summary>
        /// <param name="wrd">Удаляемое слово</param>
        /// <returns>TRUE, если слово было удалено, FALSE в противном случае</returns>
        public bool Delete(string wrd)
        {
            // если слово закончилось, проверяем, есть ли в этом узле точка
            if (wrd.Equals(string.Empty))
            {
                bool res = Point;
                Point = false;
                return res;
            }
            if (_next[GetIndex(wrd[0])] != null)
                if (_next[GetIndex(wrd[0])].Delete(wrd.Remove(0, 1)))
                {
                    if (_next[GetIndex(wrd[0])].IsEmpty())
                        _next[GetIndex(wrd[0])] = null;
                    return true;
                } 

            return false;
        } 

        /// <summary>
        /// Функция получает список всех слов, хранящихся в дереве
        /// </summary>
        /// <param name="sl">Результирующий список слов</param>
        /// <param name="wrd">Накапливает слово. При вызове следует оставить значение по умлочанию</param>
        public void Show(List<string> sl, string wrd = "")
        {
            if (Point)
                sl.Add(wrd);
            for (int i = 0; i < Range; i++)
                if (_next[i] != null)
                    _next[i].Show(sl, wrd + GetSymb(i));
        } 

        /// <summary>
        /// Поиск слов, содержащих только символы из указанного множества
        /// </summary>
        /// <param name="set">Множество символов</param>
        /// <param name="sl">Результирующий список слов</param>
        /// <param name="wrd">Параметр, накапливающий слово в рекурсии. При вызове оставить пустым</param>
        public void ShowSet(HashSet<char> set, List<string> sl, string wrd = "")
        {
            if (Point)
                sl.Add(wrd);
            for (int i = 0; i < Range; i++)
                if (_next[i] != null && set.Contains(GetSymb(i)))
                    _next[i].ShowSet(set, sl, wrd + GetSymb(i));
        } 

        /// <summary>
        /// Функция возвращает код символа, переданного в качестве параметра. 
        /// Диапазон [от 0 до количества символов в алфавите] для корректного обращения к массиву.
        /// </summary>
        /// <param name="chr">Буква, код которой следует определить</param>
        /// <returns>Код-позиция символа в массиве, индексами которого служат символы алфавита</returns>
        private static int GetIndex(char chr)
        {
            return chr - FirstLitera;
        }      

        /// <summary>
        /// Функция возвращает символ из алфавита, соответствующий номеру ячейки массива
        /// </summary>
        /// <param name="i">Номер ячейки массива</param>
        /// <returns>Символ алфавита, поставленный в соответствие номеру ячейки</returns>
        public static char GetSymb(int i)
        {
            return (char)(i + FirstLitera);
        }
    } 
}
