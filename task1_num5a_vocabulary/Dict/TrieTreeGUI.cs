﻿using System.Windows.Forms;

namespace task2_num10_noodle_game.Dict
{
    public class TrieTreeGUI: TrieTree
    {
        public string FileName { get; private set; }
        private bool _modified;
        private readonly TextBox _textBox;

        public TrieTreeGUI(TextBox tb)
        {
            FileName = "";
            _modified = false;
            _textBox = tb;
            Show(_textBox);
        }

        // добавление слова
        public new void Add(string word)
        {
            if (base.Add(word))
                Modified = true;
        }

        // удаление слова
        public new void Delete(string word)
        {
            if (base.Delete(word))
                Modified = true;
        }

        // сохранение словаря в файл
        public new void SaveToFile(string name)
        {
            FileName = name;
            base.SaveToFile(name);
            Modified = false;
        }

        // загрузка словаря из файла
        public new bool LoadFromFile(string name)
        {
            FileName = name;
            bool res = base.LoadFromFile(FileName);
            Show(_textBox);
            Modified = false;
            return res;
        }

        // свойство, показывающее, был ли словарь изменен
        public bool Modified
        {
            get { return _modified; }

            private set
            {
                _modified = value;
                if (value)
                    Show(_textBox);
            }
        }
    }
}
