﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using task2_num10_noodle_game.Dict;
using task2_num10_noodle_game.Properties;

namespace task2_num10_noodle_game
{
    /* Поиск в ширину для игры "Балда". Все слова, которые можно получить через к-ходов. */
     
    public partial class FormMain : Form
    {
        public static TrieTreeGUI Dict;
        private int _fieldSize;
        private const int MaxFontSize = 25;
        private readonly GameField _game;
        private bool _isInitialWord;

        public FormMain()
        {
            InitializeComponent();
            fdSave.InitialDirectory = Directory.GetCurrentDirectory();
            fdOpen.InitialDirectory = fdSave.InitialDirectory;
            _fieldSize = (int) numSize.Value;
            dgvField.DefaultCellStyle.Font = new Font("Arial", MaxFontSize - _fieldSize + 5);
            _game = new GameField();
            Application.Idle += MainIdle;
        }

        private void MainIdle(object sender, EventArgs e)
        {
            lblDict.Visible = lblSize.Visible = lblWord.Visible = numSize.Visible = tbWord.Visible = 
                tbDictionary.Visible = dgvField.Visible = btnOk.Visible = 
                lblMoves.Visible = numMoves.Visible = 
                addToolStripMenuItem.Enabled = saveAsToolStripMenuItem.Enabled =
                saveToolStripMenuItem.Enabled = Dict != null;

            deleteToolStripMenuItem.Enabled = searchToolStripMenuItem.Enabled = 
                runToolStripMenuItem.Enabled = Dict != null && !Dict.IsEmpty();

            tbWord.MaxLength = _fieldSize;
            numMoves.Maximum = _fieldSize * (_fieldSize - 1); // _fieldSize*_fieldSize - _game.CountLetters;  
        }

        // открытие существующего файла
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WasModified(Dict, fdSave);
            fdOpen.FileName = "";
            if (fdOpen.ShowDialog() == DialogResult.OK)
            {
                Dict = new TrieTreeGUI(tbDictionary);
                ResizeField();
                InitField();
                //_game.Resize(_fieldSize, tbWord.Text, (int)numMoves.Value);
                if (!Dict.LoadFromFile(fdOpen.FileName))
                {
                    MessageBox.Show(@"File loading error");
                    Dict = null;
                }
            }
        }

        // сохранение файла под тем же именем
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveTrieTree(Dict, fdSave);
        }

        // сохранение файла под другим именем
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fdSave.FileName = Dict.FileName;
            if (fdSave.ShowDialog() == DialogResult.OK)
                Dict.SaveToFile(fdSave.FileName);
        }

        // выход из программы
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        // добавление слова
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInput dialog = new FormInput("Add");
            if (dialog.ShowDialog() == DialogResult.OK)
                Dict.Add(dialog.Word);
        }

        // поиск слова
        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInput dialog = new FormInput("Search");
            if (dialog.ShowDialog() == DialogResult.OK)
                if (Dict.Find(dialog.Word))
                    MessageBox.Show(Resources.dictContains + dialog.Word + Resources.endingSymbols);
                else
                    MessageBox.Show(Resources.dictDoesNotContains + dialog.Word + Resources.endingSymbols);
        }

        // удаление слова
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInput dialog = new FormInput("Delete");
            if (dialog.ShowDialog() == DialogResult.OK)
                Dict.Delete(dialog.Word);
        }

        // решение задачи
        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // если начальное слово выбрано
            if (tbWord.Text.Length == _fieldSize && _game.CountLetters >= _fieldSize)
            {
                // проверяем, можем ли мы сделать заданное число ходов
                if (_game.CountLetters < _fieldSize*_fieldSize - numMoves.Value)
                {
                    _game.ChangeParams(tbWord.Text, (int) numMoves.Value);
                    
                    // получили все слова, которые можно получить через numMoves ходов
                    var result = _game.GetSolve();
                    var solve = result;
                    if (!solve.IsEmpty())
                    {
                        // сохраняем искомые слова в текстовый файл
                        solve.Delete(tbWord.Text);
                        solve.SaveToFile("result.txt");
                        MessageBox.Show(Resources.resultSaved);
                    }
                    else
                        MessageBox.Show(@"No words was found.");
                }
                else
                    MessageBox.Show(@"You can't do " + numMoves.Value + @" moves.");
            }
            else
                MessageBox.Show(@"Please, enter the initial word.");
        }

        // сохранение словаря в файл
        private static void SaveTrieTree(TrieTreeGUI tree, SaveFileDialog dialog)
        {
            if (tree == null)
            {
                MessageBox.Show(@"Dictionary is empty");
                return;
            }
            if (tree.FileName == "")
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                    tree.SaveToFile(dialog.FileName);
            }
            else
                tree.SaveToFile(tree.FileName);
        }

        // функция проверки, был ли изменен словарь
        private static void WasModified(TrieTreeGUI tree, SaveFileDialog dialog)
        {
            if (tree != null && tree.Modified)
            {
                DialogResult dl = MessageBox.Show(@"Save changes?", "", MessageBoxButtons.YesNo);
                if (dl == DialogResult.Yes)
                    SaveTrieTree(tree, dialog);
            }
        }

        // событие при закрытии формы
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dl = MessageBox.Show(@"Do you really want to close window?", "", MessageBoxButtons.YesNo);
            if (dl == DialogResult.Yes)
                WasModified(Dict, fdSave);
            else
                e.Cancel = true;
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Resources.task_condition, @"Task condition");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Володина Маргарита, 3 курс 9 группа", @"About developers");
        }

        // обработка событися выбора начального слова
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (tbWord.Text.Length == _fieldSize)
            {
                _game.Resize(_fieldSize, tbWord.Text, (int)numMoves.Value);
                ShowInitialWord();
                dgvField.Enabled = true;
            }
            else
            {
                MessageBox.Show(@"The word should have a length equal to " + _fieldSize);
                tbWord.Clear();
            }
        }

        // обработка изменения размера поля
        private void numSize_ValueChanged(object sender, EventArgs e)
        {
            // очищаем начальное слово
            tbWord.Clear();
            // очищаем существующее поле
            InitField();

            int num = _fieldSize / 2;
            for (int i = 0; i < _fieldSize; ++i)
                dgvField[i, num].Style.BackColor = Color.White;

            // меняем размер поля
            _fieldSize = (int)numSize.Value;
            // меняем размер шрифта для ячеек
            dgvField.DefaultCellStyle.Font = new Font("Arial", MaxFontSize - _fieldSize + 5);
            // перерисовываем таблицу
            ResizeField();

            dgvField.Enabled = false;
        }

        // обработка ввода начального слова: запрещаем вводить недопустимые символы
        private void tbWord_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !CheckKey(e);
        }

        // обработка ввода начального слова: когда слово меняется, делаем поле недоступным, пока не будет нажата кнопка ОК
        private void tbWord_TextChanged(object sender, EventArgs e)
        {
            dgvField.Enabled = false;
        }

        // функция проверки символа на корректность
        // символ должен принадлежать заданному диапазону, так же мы разрешаем редактирование слова с помощью Backspace и Delete
        private static bool CheckKey(KeyPressEventArgs e)
        {
            return e.KeyChar >= (char) Node.FirstLitera && e.KeyChar <= (char) Node.LastLitera ||
                    e.KeyChar == (char) Keys.Delete || e.KeyChar == (char) Keys.Back;
        }

        // обработка ввода в ячейке таблицы: проверяем вводимые символы на корректность
        private void dgvField_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += tbWord_KeyPress;
        }

        // обработка ввода новой буквы в ячейку поля игры
        private void dgvField_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!_isInitialWord)
            {
                int col = e.ColumnIndex, row = e.RowIndex;
                string letter = dgvField[col, row].Value.ToString();
                if (letter != string.Empty && _game.CanWrite(row, col))
                {
                    _game.AddLetter(row, col, letter[0]);
                    dgvField[col, row].ReadOnly = true;
                }
                else
                    dgvField[col, row].Value = string.Empty;
            }
        }

        // функция, которая при изменении размера поля меняет и его параметры
        private void ResizeField()
        {
            // создаем квадратное поле для игры
            dgvField.RowCount = dgvField.ColumnCount = _fieldSize;

            int cellSize = dgvField.Height/dgvField.RowCount;
            for (int i = 0; i < _fieldSize; ++i)
                dgvField.Columns[i].Width = dgvField.Rows[i].Height = cellSize;

            // разрешаем пользователю вводить только одну букву в каждой ячейке
            for (int i = 0; i < _fieldSize; ++i)
                ((DataGridViewTextBoxColumn)dgvField.Columns[i]).MaxInputLength = 1;

            // делаем все ячейки поля доступными для ввода
            for (int i = 0; i < _fieldSize; ++i)
                for (int j = 0; j < _fieldSize; ++j)
                    dgvField[i, j].ReadOnly = false;

            int num = _fieldSize / 2;
            for (int i = 0; i < _fieldSize; ++i)
            {
                // центрируем буквы в каждой колонке поля
                dgvField.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                // выделяем строку поля, в которой записано начальное слово для игры
                dgvField[i, num].Style.BackColor = Color.Bisque;
                // делаем строку поля, в которой записано начальное слово, доступной только для чтения
                dgvField[i, num].ReadOnly = true;
            }
        }

        // функция отображает начальное слово на поле 
        private void ShowInitialWord()
        {
            int num = _fieldSize/2;
            _isInitialWord = true;
            for (int i = 0; i < _fieldSize; ++i)
                dgvField[i, num].Value = tbWord.Text[i];
            _isInitialWord = false;
        }

        // инициализация поля для игры
        private void InitField()
        {
            // очистка всего поля
            for (int i = 0; i < _fieldSize; ++i)
                for (int j = 0; j < _fieldSize; ++j)
                    dgvField[i, j].Value = string.Empty;
        }
    }
}
